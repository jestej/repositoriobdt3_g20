PGDMP         "                y            Tarea1BD    12.6    12.6     ,           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            -           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            .           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            /           1262    16767    Tarea1BD    DATABASE     �   CREATE DATABASE "Tarea1BD" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Chile.1252' LC_CTYPE = 'Spanish_Chile.1252';
    DROP DATABASE "Tarea1BD";
                postgres    false                        3079    16768 	   adminpack 	   EXTENSION     A   CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;
    DROP EXTENSION adminpack;
                   false            0           0    0    EXTENSION adminpack    COMMENT     M   COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';
                        false    1            �            1259    16777    cuenta_bancaria    TABLE     �   CREATE TABLE public.cuenta_bancaria (
    numero_cuenta integer NOT NULL,
    id_usuario integer NOT NULL,
    balance numeric NOT NULL
);
 #   DROP TABLE public.cuenta_bancaria;
       public         heap    postgres    false            �            1259    16783    moneda    TABLE     �   CREATE TABLE public.moneda (
    id integer NOT NULL,
    sigla character varying(10) NOT NULL,
    nombre character varying(80) NOT NULL
);
    DROP TABLE public.moneda;
       public         heap    postgres    false            �            1259    16786    pais    TABLE     l   CREATE TABLE public.pais (
    cod_pais integer NOT NULL,
    nombre_pais character varying(45) NOT NULL
);
    DROP TABLE public.pais;
       public         heap    postgres    false            �            1259    16789    precio_moneda    TABLE     �   CREATE TABLE public.precio_moneda (
    id_moneda integer NOT NULL,
    fecha timestamp without time zone NOT NULL,
    valor numeric NOT NULL
);
 !   DROP TABLE public.precio_moneda;
       public         heap    postgres    false            �            1259    16795    usuario    TABLE     9  CREATE TABLE public.usuario (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(50),
    correo character varying(50) NOT NULL,
    "contraseña" character varying(50) NOT NULL,
    pais integer NOT NULL,
    fecha_registro timestamp without time zone NOT NULL
);
    DROP TABLE public.usuario;
       public         heap    postgres    false            �            1259    16798    usuario_tiene_moneda    TABLE     �   CREATE TABLE public.usuario_tiene_moneda (
    id_usuario integer NOT NULL,
    id_moneda integer NOT NULL,
    balance numeric NOT NULL
);
 (   DROP TABLE public.usuario_tiene_moneda;
       public         heap    postgres    false            $          0    16777    cuenta_bancaria 
   TABLE DATA           M   COPY public.cuenta_bancaria (numero_cuenta, id_usuario, balance) FROM stdin;
    public          postgres    false    203   �!       %          0    16783    moneda 
   TABLE DATA           3   COPY public.moneda (id, sigla, nombre) FROM stdin;
    public          postgres    false    204   �"       &          0    16786    pais 
   TABLE DATA           5   COPY public.pais (cod_pais, nombre_pais) FROM stdin;
    public          postgres    false    205   �#       '          0    16789    precio_moneda 
   TABLE DATA           @   COPY public.precio_moneda (id_moneda, fecha, valor) FROM stdin;
    public          postgres    false    206   D$       (          0    16795    usuario 
   TABLE DATA           d   COPY public.usuario (id, nombre, apellido, correo, "contraseña", pais, fecha_registro) FROM stdin;
    public          postgres    false    207   t+       )          0    16798    usuario_tiene_moneda 
   TABLE DATA           N   COPY public.usuario_tiene_moneda (id_usuario, id_moneda, balance) FROM stdin;
    public          postgres    false    208   �/       �
           2606    16805 $   cuenta_bancaria cuenta_bancaria_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_pkey PRIMARY KEY (numero_cuenta);
 N   ALTER TABLE ONLY public.cuenta_bancaria DROP CONSTRAINT cuenta_bancaria_pkey;
       public            postgres    false    203            �
           2606    16807    moneda moneda_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.moneda
    ADD CONSTRAINT moneda_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.moneda DROP CONSTRAINT moneda_pkey;
       public            postgres    false    204            �
           2606    16809    pais pais_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (cod_pais);
 8   ALTER TABLE ONLY public.pais DROP CONSTRAINT pais_pkey;
       public            postgres    false    205            �
           2606    16811     precio_moneda precio_moneda_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.precio_moneda
    ADD CONSTRAINT precio_moneda_pkey PRIMARY KEY (id_moneda, fecha);
 J   ALTER TABLE ONLY public.precio_moneda DROP CONSTRAINT precio_moneda_pkey;
       public            postgres    false    206    206            �
           2606    16813    usuario usuario_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pkey;
       public            postgres    false    207            �
           2606    16815 .   usuario_tiene_moneda usuario_tiene_moneda_pkey 
   CONSTRAINT        ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_pkey PRIMARY KEY (id_usuario, id_moneda);
 X   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_pkey;
       public            postgres    false    208    208            �
           2606    16816 /   cuenta_bancaria cuenta_bancaria_id_usuario_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 Y   ALTER TABLE ONLY public.cuenta_bancaria DROP CONSTRAINT cuenta_bancaria_id_usuario_fkey;
       public          postgres    false    207    203    2718            �
           2606    16821 *   precio_moneda precio_moneda_id_moneda_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.precio_moneda
    ADD CONSTRAINT precio_moneda_id_moneda_fkey FOREIGN KEY (id_moneda) REFERENCES public.moneda(id);
 T   ALTER TABLE ONLY public.precio_moneda DROP CONSTRAINT precio_moneda_id_moneda_fkey;
       public          postgres    false    206    2712    204            �
           2606    16826    usuario usuario_pais_fkey    FK CONSTRAINT     z   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pais_fkey FOREIGN KEY (pais) REFERENCES public.pais(cod_pais);
 C   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pais_fkey;
       public          postgres    false    205    207    2714            �
           2606    16831 8   usuario_tiene_moneda usuario_tiene_moneda_id_moneda_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_id_moneda_fkey FOREIGN KEY (id_moneda) REFERENCES public.moneda(id);
 b   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_id_moneda_fkey;
       public          postgres    false    208    204    2712            �
           2606    16836 9   usuario_tiene_moneda usuario_tiene_moneda_id_usuario_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 c   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_id_usuario_fkey;
       public          postgres    false    208    207    2718            $   �   x��ˑ1C�Ո.�+O��gl	�@
��jP��Y���t�Z�7m����=�~���,G�ڣ`z2�m��	+��-��A{X�:^�[�z��{���'a"ۜ��`M3F@r/�\}�C&��鰏0i�N��@~6��~Թ�����n��8g���G��v�HaD{��)J�qkCvr��'�0$6��4GnZ��D߲q�O^6��\�&6����Y���g���sD�      %   �   x�-�K�0E������!P"�0��I�h-���ݓ���v�Ն6���kx੣-��L����A�.����=�
��Y�erE�m��%S�f�#�,E���e:!��p������N��.�b��b����Fy o�K����>B5����\�%{:      &   �   x��;�0��S��|���$Z�*6���k��6�S�bl5o^1-Lխߧ���KL��Ø��͝��0�\�b <����6�'�|4S<��f���F�GG�*��t�}��q/)��jf�T��Z�,�K`��B<{|l�%"3      '      x�]X[�#;���"Hʀ�����u\	�}��1U�ccB��Ӣ�S���w�Kۥ�k��ߘ��_r ��.-W+7Dz���w)� �nH� �w�-�~ ݾ�P���^vN�k}%A���@ )�Yg\��[/j���֍�Oя�G��&�AvB���w�ԫ�8���Yo�z�OoHi�"�hc(�kސ�v?��߻���Ң/�e�}��"�HN,w�E��A%��p5AiAN4��o;�Ir�"���� ��j���7���Y1ֹ�%A��"꣈岅���H܍@b�[�U��Sñ��� +(��I�6�`q��"aNj;��D�b�Y���*�N�2+&W�P���.��>-A�-$�]v�2��O�\�L-�p 2��d@�_� mb�XI�6F�v��t�k	���_�v�V���8����Õ&|��n�B���qI���O�T�&�t��X�~G��� =���7DU���Ӎ2��-�2���.t!.ҕ�B��~��)�&��(5=���9��=r� ��P��XS��8�ge���]�J$N�>��"6��J��R�{V;�!�
�,�6ҿ�H���s.*v_����C�U,.�;-[q=ql���2�G;�)��e^:���V���Ғ���e6o�J)2��7[M��9����e���&ʧ@p�H�UH�zȂ�������0�s��XlX:ey�÷�B/пg�p9�W-������W��̓:Yl�~�,^$$]�,v+�p!�-APE�;t���(���|.�?"��Eh �r=dy�XĻ�<�)��ց��Xc�Be�Ų�3��y��Pj7�b�(��K��$���1��3����:/�S�) �h� �H�S�#[Qk�塨t�n����]��M��\����B]v�h�h�tʤY�)���
�;uya���.�i�z+��j��[�%��U:��0u���D(Sײ�ܧ�d�h:ڃޅ�K�����m�!Ldɐ��07�V�ezS�[�4�@�Y,cjz�!�!���� ��\�IJ��n$_���5��q�:��g�I��^�&��}R�΍n;�拔MR���n�k�c�Tp���7�9E�ݩ�.���Y�7d�2bz(�H7ڝGиW�?�r�0��F����Q3's�Oma�U�[�4@����b�����!��!�5���@|�C}�n�s[���h<%5�h=����f�0���!\9�AP!ې-��z9S�\��͖�W�F1L��X�H=�yQC���}�
=����.��\_#��=5N	�1����l�"�*W��fK�D��SI�2��th�8e�3W�0�%!3����J��̿�H.��=����f��3B8�QIޥ�F�@���7�6��K��h�D�8Eƣ��K�[�8�o�=;͢>�)�Ž]�v#�d�x���{�G[���(rK!J~	QJnIõ'HcѭUR�:�Y_3����^ 
5d�� %͙�{v@ح�%!>b���t>����$Y����OT~�y�"tQ���5MҮkD�{[W������<l��4-QAa�K�p�O775E�;d�5�@���?a蝕�������ѺJ��G��k��\���+�j�m��b+���RGޮ�ھ�B�#�A�d��-:oz}p�\�J��
%�b�"s��Gˇq��=k��4N����3~:X'/���̅��.H�:���K,m�>��������D*)�1ͯi�N�xp0{�VJj4Rm����O���9���>���Z=�lb��f�T�F�%K�P�I=c�Zqp�F����\�)���"a�A�ߤ�ϟ���p8~Wݎ:!�����z���4��      (     x�U��n�6��'O�0AR�2u�EӴ��EozCɴ�5-e%����Jބc>�=�FJ7�q�gw��nr��m�K7��.�� u����.��2�alC�o�>.�O�߹~?�i�#��pv}X��&ϩ��n��!�JzC���JT���L=<��x�a�29�����8.z���LY�
dԓ�ֆ�O=/��r|�0������SY��֬)�JJP��ĝF���Ww��e��H�7܎o�0��E�n��δ̵�Ԇ��%<ݮ��PL���1iޞ�aU�)gUIin��ѱ4;�3�:F��~�p^��.��?+m
�RQ	&���$ŧ���㈵���9�d��##���T�6/90z+�o8�1P�������E"�RCXƍ妴�b� QKm�D�����i�c��{����kiYi^�L�}G#�3�"!��м��'x_��J�UU9/K���%��cb<������-���û��Vּj��z]�|��I�2�?�P��a����7��W�i��F7%�Zg�J�:���C�໿�>v+�)R�%���3��f��j���9���p���.����h�2
�dm��Sdv/$�K'L���%�+&ݍ�	�Z�����M�D�S�(*xqg7\\�f�"���+j-Ѹ�Z���'�)R�j�2����gwť��˃���6��0
Ϋ�Z�ua
���1����O��?�	��}2,���*-��gx�ֹ�8
�l:��ԟ[?a����M;8��i}��;+���\[e�X.��Mvxw��3��6�w���iu&�ůӁ��>����|`�w>�IRB�BڲΕ4��;�Q�J�2��Ͼ��v��q�Hĭ��DW�TE�^�,Y��&b���	�uC���9T-�ͅ�U�o�5q�Y*����z��xc{��Q�M��
�
�d�n�YNPbY.��}��e�y��H�n_�;]����O3y;�
�Z#��rCU��h�B��x&Z���W���<�+�?R�(��?������+      )   �   x�%�ɍE1�2;�2��1���E��Q��V6J
�2ޓPQ�d��>����	���D���iq�^��IJ>v��R�yd+�@Aݲ�MKn�
ʔ�����`�5,��+F_�mqc5��::�J4k���6������w.T���ƫx���O�+���H�-sL��!9�oF�a��,��:$�c���þ[�)N� /0W|6� �B�{|V|���9��O��@�G     