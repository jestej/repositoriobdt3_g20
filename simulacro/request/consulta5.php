<?php


if(isset($_POST['c5'])){
    $varfilter = $_POST['id_moneda'];
    $url = 'localhost:5000/api/consultas/5/'.$varfilter;
    $ch= curl_init($url);
    
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

    $response = curl_exec($ch);
    
    if (curl_error($ch)){
        echo curl_error($ch);
    }
    else{ 
        $decodedc5= json_decode($response,true);
    }
    $contadorusuario5 = count($decodedc5['valor_maximo']);
    $o=0;
    $maxx=0;
    while($o < $contadorusuario5){
        $maxx += $decodedc5['valor_maximo'][$o]['balance'];
        $o++;
    }
      
}   




?>

<!DOCTYPE html>
<html>
    <head>
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Tarea 3 - Grupo 20</title>
                <!-- Librerías BootStrap -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
            <!-- Iconos FontAwesome -->
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
            <!-- Elementos del Estilo -->
        </head>
    </head>
    <body>
        <div class='container-fluid'>
            <div class="row p-3">
                <h1>Consulta 5</h1>
            </div>
            <div class="row p-3">
                <div class="col">
                    <div class="container shadow-lg rounded m-auto p-5">
                    <table class="table table-hover table-dark">
                            <tr>
                                <th>Moneda</th>
                                <th>Cantidad Total</th>
                              </tr>
                              
                            <tr>
                                <td> <?php echo $decodedc5['valor_maximo'][1]['nombre']; ?> </td>
                                <td>  <?php echo $maxx; ?></td> 
                            </tr>
                            
                        </table>
                        <a type="button" class="btn btn-outline-info" href="consultas.html">Volver</a>
                    </div>
                </div>
            </div>
            <?php curl_close($ch);?>
        </div>
    </body>
</html>