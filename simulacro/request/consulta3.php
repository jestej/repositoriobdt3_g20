<?php


if(isset($_POST['c3'])){
    $pais = $_POST['pais'];
    $url = 'localhost:5000/api/consultas/3/'.$pais;
    $ch= curl_init($url);
    
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

    $response = curl_exec($ch);
    
    if (curl_error($ch)){
        echo curl_error($ch);
    }
    else{ 
        $decodedc3= json_decode($response,true);
    }
    $contadorusuario3 = count($decodedc3['usuario']);
      
}   




?>

<!DOCTYPE html>
<html>
    <head>
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Tarea 3 - Grupo 20</title>
                <!-- Librerías BootStrap -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
            <!-- Iconos FontAwesome -->
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
            <!-- Elementos del Estilo -->
        </head>
    </head>
    <body>
        <div class='container-fluid'>
            <div class="row p-3">
                <h1>Consulta 3</h1>
            </div>
            <div class="row p-3">
                <div class="col">
                    <div class="container shadow-lg rounded m-auto p-5">
                    <table class="table table-hover table-dark">
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Correo</th>
                                <th>Pais</th>
                                <th>Fecha Registro</th>
                              </tr>
                              <?php for($i=0;$i < $contadorusuario3 ; $i++){ ?>
                            <tr>
                                <td> <?php echo $decodedc3['usuario'][$i]['id']; ?> </td>
                                <td>  <?php echo $decodedc3['usuario'][$i]['nombre']; ?></td>
                                <td> <?php echo $decodedc3['usuario'][$i]['apellido']; ?> </td>
                                <td><?php echo $decodedc3['usuario'][$i]['correo']; ?></td>
                                <td><?php echo $decodedc3['usuario'][$i]['nombre_pais']; ?></td>
                                <td><?php echo $decodedc3['usuario'][$i]['fecha_registro']; ?></td>      
                             
                            </tr>
                            <?php }?>
                        </table>
                        <a type="button" class="btn btn-outline-info" href="consultas.html">Volver</a>
                    </div>
                </div>
                <?php curl_close($ch);?>
            </div>
        </div>
    </body>
</html>