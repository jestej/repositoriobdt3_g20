<?php 
include '../read/readuser.php';
function callAPI($method, $url, $data){
   $curl = curl_init();
   switch ($method){
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data){
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         }			 					
         break;
      default:
         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   }
   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   // EXECUTE:
   $result = curl_exec($curl);
   if(!$result){
      die("Connection Failure");
   }
   curl_close($curl);
   return $result;
}
$select_user= 'http://localhost:5000/api/usuario';
//Leemos el dato para mostrarlo en Placeholder
if(isset($_POST['updatee_user'])){
   $id_update = $_POST['idup'];
   $nombre_update= $_POST['nombreup'];
   $apellido_update = $_POST['apellidoup'];
   $correo_update = $_POST['correoup'];
   $pwd_update= $_POST['pwdup'];
   $pais_update = $_POST['paisup'];
   $fecha_update = $_POST['fechaup'];
}

if(isset($_POST['update_user'])){
   $id= $_POST['id_upda'];
   $nombre = $_POST['nombre_upda'];
   $apellido = $_POST['apellido_upda'];
   $correo = $_POST['correo_upda'];
   $pwd = md5($_POST['pwd_upda']);
   $pais = $_POST['pais_upda'];
   $fecha_registro = $_POST['fecha_upda'];

   $updateuser_array = array(
      "id"=> $id,
      "nombre" => $nombre,
      "apellido" => $apellido,
      "correo"=> $correo,
      "contraseña" => $pwd,
      "pais" => $pais,
      "fecha_registro" => $fecha_registro
   );
   $urluser = $select_user.'/'.$id;
   $updateuser = callAPI('PUT',$urluser, json_encode($updateuser_array));
   header("location: ../crud.html");

}
?>