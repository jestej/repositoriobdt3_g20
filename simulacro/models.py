from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text,exc,func
from sqlalchemy.orm import backref, relationship
from sqlalchemy import exc
import sys

db = SQLAlchemy()

class Usuario(db.Model):
	__tablename__ = 'usuario'
	id = db.Column(db.Integer,  primary_key=True)
	nombre = db.Column(db.String(50), nullable=False)
	apellido = db.Column(db.String(50), nullable=True)
	correo = db.Column(db.String(50), nullable=False)
	contraseña = db.Column(db.String(50), nullable=False)
	pais = db.Column(db.Integer, db.ForeignKey('pais.cod_pais'))
	fecha_registro = db.Column(db.DateTime(), nullable=False, default=db.func.current_timestamp())
	
	cuenta_bancaria = db.relationship('Cuenta_bancaria', cascade="all,delete", backref="usuario")
	usuario_tiene_moneda = db.relationship('Usuario_tiene_moneda', cascade="all,delete", backref="usuario")
	

	@classmethod
	def create(cls, _id ,_nombre, _apellido, _correo, _contraseña, _pais,_fecha_registro):
		_usuario = Usuario(id=_id,nombre=_nombre, apellido=_apellido, correo=_correo, contraseña=_contraseña, pais=_pais,fecha_registro =_fecha_registro)
		return _usuario.save()

	def save(self):
		try:
			db.session.add(self)
			db.session.commit()
			return self
		except exc.IntegrityError:
			print("Oops!", sys.exc_info(), "occurred.")
			return False

	def json(self):
		return {
			'id': self.id,
			'nombre': self.nombre,
			'apellido': self.apellido,
			'correo': self.correo,
			'contraseña': self.contraseña,
			'pais': self.pais,
			'fecha_registro': self.fecha_registro.strftime('%Y-%m-%d %H:%M:%S.%f')
		}

	def update(self):
		self.save()

	def delete(self):
		try:
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False
	# Consulta 1
	def getFecha_registrorequest(year):
		try:
			result = db.session.execute('SELECT * FROM  "usuario" where extract(year from "fecha_registro") = :fecha_registro', {'fecha_registro': year})
			return result
		except:
			return False
	#Consulta 3
	def getUserByCountry(cod_pais):
		try:
			result = db.session.execute('SELECT * FROM usuario FULL OUTER join pais on pais=cod_pais where cod_pais= :pais', {'pais': cod_pais})
			return result
		except:
			return False


class Pais(db.Model):
	__tablename__ = 'pais'
	cod_pais = db.Column(db.Integer, primary_key=True)
	nombre_pais = db.Column(db.String(45), nullable=False)

	

	@classmethod
	def create(cls, _cod_pais ,_nombre_pais):
		_pais = Pais(cod_pais= _cod_pais ,nombre_pais=_nombre_pais)
		return _pais.save()

	def save(self):
		try:
			db.session.add(self)
			db.session.commit()
			return self
		except exc.IntegrityError:
			print("Oops!", sys.exc_info(), "occurred.")
			return False

	def json(self):
		return {
			'cod_pais': self.cod_pais,
			'nombre_pais': self.nombre_pais
		}

	def update(self):
		self.save()

	def delete(self):
		try: 
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False

class Cuenta_bancaria(db.Model):
	__tablename__ = 'cuenta_bancaria'
	numero_cuenta = db.Column(db.Integer, primary_key=True)
	id_usuario = db.Column(db.Integer, db.ForeignKey('usuario.id'))
	balance = db.Column(db.Float, nullable=False)

	@classmethod
	def create(cls,_numero_cuenta, _id_usuario, _balance):
		_cuenta = Cuenta_bancaria(numero_cuenta=_numero_cuenta ,id_usuario=_id_usuario, balance=_balance)
		return _cuenta.save()

	def save(self):
		try:
			db.session.add(self)
			db.session.commit()
			return self
		except exc.IntegrityError:
			print("Oops!", sys.exc_info(), "occurred.")
			return False

	def json(self):
		return {
			'numero_cuenta': self.numero_cuenta,
			'id_usuario': self.id_usuario,
			'balance': self.balance
		}

	def update(self):
		self.save()

	def delete(self):
		try:
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False

	def getByBalance(selbalance):		
		try:
			result = db.session.execute('SELECT * FROM  "cuenta_bancaria" where "balance" > :balance', {'balance': selbalance})
			return result
		except:
			return False

class Moneda(db.Model):
	__tablename__ = 'moneda'
	id = db.Column(db.Integer, primary_key=True)

	sigla = db.Column(db.String(10), nullable=False)
	nombre = db.Column(db.String(80), nullable=False)

	usuario_tiene_moneda = db.relationship('Usuario_tiene_moneda', cascade="all,delete", backref="moneda")
	precio_moneda = db.relationship('Precio_moneda', cascade="all,delete", backref="moneda")

	@classmethod
	def create(cls,_id , _sigla, _nombre):
		_moneda = Moneda(id=_id,sigla=_sigla, nombre=_nombre)
		return _moneda.save()

	def save(self):
		try:
			db.session.add(self)
			db.session.commit()
			return self
		except exc.IntegrityError:
			print("Oops!", sys.exc_info(), "occurred.")
			return False

	def json(self):
		return {
			'id': self.id,
			'sigla': self.sigla,
			'nombre': self.nombre
		}

	def update(self):
		self.save()

	def delete(self):
		try:
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False
	def getMaxByCoin(_moneda):
		print(_moneda)
		max = Moneda.query.filter(Moneda.nombre == _moneda)
		max = Moneda.query(func.max(Moneda.precio_moneda)).filter(Moneda.nombre == _moneda)
		return max

class Usuario_tiene_moneda(db.Model):
	__tablename__ = 'usuario_tiene_moneda'
	id_usuario = db.Column(db.Integer, db.ForeignKey('usuario.id'), primary_key=True)
	id_moneda = db.Column(db.Integer, db.ForeignKey('moneda.id'), primary_key=True)
	balance = db.Column(db.Float, nullable=False)

	@classmethod
	def create(cls, _id_usuario, _id_moneda, _balance):
		_usuario = Usuario_tiene_moneda(id_usuario=_id_usuario, id_moneda=_id_moneda, balance=_balance)
		return _usuario.save()

	def save(self):
		try:
			db.session.add(self)
			db.session.commit()
			return self
		except exc.IntegrityError:
			print("Oops!", sys.exc_info(), "occurred.")
			return False

	def json(self):
		return {
			'id_usuario': self.id_usuario,
			'id_moneda': self.id_moneda,
			'balance': self.balance
		}

	def update(self):
		self.save()

	def delete(self):
		try:
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False
	#Consulta 5
	def getMaxUserCoin(id_moneda):
		try:
			result = db.session.execute('SELECT * FROM usuario_tiene_moneda INNER JOIN moneda ON usuario_tiene_moneda.id_moneda = moneda.id WHERE id_moneda = :id ' ,{'id': id_moneda})
			return result
		except:
			return False
	#Consulta 6
	def getTop3():
		try:
			result = db.session.execute('SELECT Moneda.nombre as Nombre, count(Usuario_tiene_moneda.id_usuario) as Cantidad_Usuarios FROM Usuario_tiene_moneda INNER JOIN Moneda ON Moneda.id = Usuario_tiene_moneda.id_moneda GROUP BY Moneda.nombre ORDER BY count(Usuario_tiene_moneda.id_usuario) desc Limit 3')
			return result
		except:
			return False
	#Consulta 8
	def getMaxcoin(id_usuario):
		try:
			result = db.session.execute('SELECT DISTINCT u.nombre as "Nombre", u.apellido as "Apellido", m.nombre as "Nombre Moneda", utm.balance as "Cantidad" FROM usuario as u LEFT JOIN usuario_tiene_moneda as utm ON u.id = utm.id_usuario LEFT JOIN moneda as m ON utm.id_moneda = m.id  WHERE u.id = :id ORDER BY utm.balance desc limit 1',{'id': id_usuario})
			return result
		except:
			return False
class Precio_moneda(db.Model):
	__tablename__ = 'precio_moneda'
	id_moneda = db.Column(db.Integer, db.ForeignKey('moneda.id'), primary_key=True)
	fecha = db.Column(db.DateTime(), primary_key=True, default=db.func.current_timestamp())
	valor = db.Column(db.Float, nullable=False)

	@classmethod
	def create(cls, _id_moneda,_fecha ,_valor):
		_pm = Precio_moneda(id_moneda=_id_moneda,fecha=_fecha, valor=_valor)
		return _pm.save()

	def save(self):
		try:
			db.session.add(self)
			db.session.commit()
			return self
		except exc.IntegrityError:
			print("Oops!", sys.exc_info(), "occurred.")
			return False

	def json(self):
		return {
			'id_moneda': self.id_moneda,
			'fecha': self.fecha.strftime('%Y-%m-%d %H:%M:%S'),
			'valor': self.valor
		}
		

	def update(self):
		self.save()

	def delete(self):
		try:
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False
	#Consulta 4
	def getMaxByCoin(id_moneda):
		try:
			result = db.session.execute('SELECT * FROM precio_moneda INNER JOIN moneda ON id_moneda = id WHERE id_moneda = :moneda ORDER BY valor desc limit 1' ,{'moneda': id_moneda})
			return result
		except:
			return False
	# Consulta 7
	def getmonthyear(year,month):
		try:
			result = db.session.execute('SELECT Moneda.nombre as "Moneda", count(EXTRACT(MONTH FROM DATE(Precio_moneda.fecha))) as "Veces que cambio valor" FROM Precio_Moneda INNER JOIN Moneda ON Moneda.id = Precio_moneda.id_moneda WHERE EXTRACT(MONTH FROM DATE(Precio_moneda.fecha)) = :month AND EXTRACT(YEAR FROM DATE(Precio_moneda.fecha)) = :year GROUP BY Moneda.nombre ORDER BY count(EXTRACT(MONTH FROM DATE(Precio_moneda.fecha))) desc Limit 1', {'month': month,'year': year})
			return result
		except :
			return False
			