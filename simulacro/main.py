from flask import Flask, json
from flask import jsonify
from flask import request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql.elements import and_
from config import config
from models import db, Usuario, Cuenta_bancaria, Pais, Moneda, Precio_moneda, Usuario_tiene_moneda


def create_app(enviroment):
	app = Flask(__name__)
	app.secret_key = "Secret Key"
	app.config.from_object(enviroment)
	with app.app_context():
		db.init_app(app)
		db.create_all()
	return app

# Accedemos a la clase config del archivo config.py
enviroment = config['development']
app = create_app(enviroment)



@app.route('/api/usuario', methods=['GET'])
def get_users():
	usuarios = [ usuario.json() for usuario in Usuario.query.all() ] 
	return jsonify({'usuarios': usuarios })

# Endpoint para obtener el usuario con id <id>
@app.route('/api/usuario/<id>', methods=['GET'])
def get_user(id):
	usuario = Usuario.query.filter_by(id=id).first()
	if usuario is None:
		return jsonify({'message': 'User does not exists'}), 404

	return jsonify({'usuario': usuario.json() })


@app.route('/api/pais', methods=['GET'])
def get_paises():
	paises = [ pais.json() for pais in Pais.query.all() ] 
	return jsonify({'Paises': paises })


@app.route('/api/cuenta_bancaria', methods=['GET'])
def get_cuentas():
	cuentas = [ cuenta.json() for cuenta in Cuenta_bancaria.query.all() ] 
	return jsonify({'Cuentas Bancarias': cuentas })

@app.route('/api/usuario_tiene_moneda', methods=['GET'])
def get_users_money():
	usuarios = [ usuario.json() for usuario in Usuario_tiene_moneda.query.all() ] 
	return jsonify({'usuarios con moneda': usuarios })

@app.route('/api/moneda', methods=['GET'])
def get_monedas():
	monedas = [ moneda.json() for moneda in Moneda.query.all() ] 
	return jsonify({'Monedas': monedas })

@app.route('/api/precio_moneda', methods=['GET'])
def get_prices():
	prices = [ price.json() for price in Precio_moneda.query.all() ] 
	return jsonify({'Precios de monedas': prices })

#########################################################################

#########################################################################
# Endpoint para insertar un usuario en la bd
@app.route('/api/usuario', methods=['POST'])
def create_user():
	json = request.get_json(force=True)
	print(json['nombre'], json['apellido'], json['correo'], json['contraseña'], json['pais'])

	if json.get('id') is None or json.get('nombre') is None or  json.get('correo') is None or json.get('contraseña') is None or json.get('pais') is None or json.get('fecha_registro') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	usuario = Usuario.create(json['id'],json['nombre'], json['apellido'], json['correo'], json['contraseña'], json['pais'],json['fecha_registro'])

	return jsonify({'usuario': usuario.json() })

@app.route('/api/pais', methods=['POST'])
def create_pais():
	json = request.get_json(force=True)

	if json.get('nombre_pais') is None or json.get('cod_pais') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	pais = Pais.create(json['cod_pais'],json['nombre_pais'])

	return jsonify({'Pais': pais.json() })

@app.route('/api/cuenta_bancaria', methods=['POST'])
def create_cuenta():
	json = request.get_json(force=True)

	if json.get('numero_cuenta') is None or json.get('id_usuario') is None or json.get('balance') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	cuenta_bancaria = Cuenta_bancaria.create(json['numero_cuenta'],json['id_usuario'], json['balance'])

	return jsonify({'Cuenta bancaria': cuenta_bancaria.json() })

@app.route('/api/moneda', methods=['POST'])
def create_coin():
	json = request.get_json(force=True)

	if json.get('id') is None or json.get('sigla') is None or json.get('nombre') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	moneda = Moneda.create(json['id'],json['sigla'], json['nombre'])

	return jsonify({'Moneda': moneda.json() })

@app.route('/api/usuario_tiene_moneda', methods=['POST'])
def create_user_money():
	json = request.get_json(force=True)

	if json.get('id_usuario') is None or json.get('id_moneda') is None or json.get('balance') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	user_money = Usuario_tiene_moneda.create(json['id_usuario'], json['id_moneda'], json['balance'])

	return jsonify({'Usuario tiene moneda': user_money.json() })

@app.route('/api/precio_moneda', methods=['POST'])
def create_coin_price():
	json = request.get_json(force=True)

	if json.get('id_moneda') is None or json.get('fecha') is None or json.get('valor') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	coin_price = Precio_moneda.create(json['id_moneda'],json['fecha'],  json['valor'])

	return jsonify({'Precio Moneda': coin_price.json() })

###############################################################################

##############################################################################
# Endpoint para actualizar los datos de un usuario en la bd
@app.route('/api/usuario/<id>', methods=['PUT'])
def update_user(id):
	usuario = Usuario.query.filter_by(id=id).first()
	if usuario is None:
		return jsonify({'message': 'User does not exists'}), 404

	json = request.get_json(force=True)
	if json.get('nombre') is None or json.get('correo') is None or json.get('contraseña') is None or json.get('pais') is None:
		return jsonify({'message': 'El formato está mal'}), 400
	usuario.id = json['id']
	usuario.nombre = json['nombre']
	usuario.apellido = json['apellido']
	usuario.contraseña = json['contraseña']
	usuario.correo = json['correo']
	usuario.pais = json['pais']
	usuario.fecha_registro = json['fecha_registro']

	usuario.update()

	return jsonify({'usuario': usuario.json() })

@app.route('/api/pais/<cod_pais>', methods=['PUT'])
def update_pais(cod_pais):
	pais = Pais.query.filter_by(cod_pais=cod_pais).first()
	if pais is None:
		return jsonify({'message': 'Country does not exists'}), 404

	json = request.get_json(force=True)
	if json.get('nombre_pais') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	pais.cod_pais = json['cod_pais']
	pais.nombre_pais = json['nombre_pais']

	pais.update()

	return jsonify({'pais': pais.json() })

@app.route('/api/cuenta_bancaria/<numero_cuenta>', methods=['PUT'])
def update_account(numero_cuenta):
	cuenta = Cuenta_bancaria.query.filter_by(numero_cuenta=numero_cuenta).first()
	if cuenta is None:
		return jsonify({'message': 'Account does not exists'}), 404

	json = request.get_json(force=True)
	if json.get('id_usuario') is None or json.get('balance') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	cuenta.id_usuario = json['id_usuario']
	cuenta.balance = json['balance']

	cuenta.update()

	return jsonify({'cuenta': cuenta.json() })

@app.route('/api/moneda/<id>', methods=['PUT'])
def update_coin(id):
	coin = Moneda.query.filter_by(id=id).first()
	if coin is None:
		return jsonify({'message': 'Account does not exists'}), 404

	json = request.get_json(force=True)
	if json.get('sigla') is None or json.get('nombre') is None:
		return jsonify({'message': 'El formato está mal'}), 400
	coin.id=json['id']
	coin.sigla = json['sigla']
	coin.nombre = json['nombre']

	coin.update()

	return jsonify({'coin': coin.json() })

@app.route('/api/usuario_tiene_moneda/<id_usuario>/<id_moneda>', methods=['PUT'])
def update_user_money(id_usuario,id_moneda):
	usuario = db.session.query(Usuario_tiene_moneda).filter(and_(Usuario_tiene_moneda.id_usuario == id_usuario , Usuario_tiene_moneda.id_moneda == id_moneda)).first()
	if usuario is None:
		return jsonify({'message': 'Account does not exists'}), 404

	json = request.get_json(force=True)
	if json.get('id_moneda') is None or json.get('balance') is None:
		return jsonify({'message': 'El formato está mal'}), 400
	usuario.id_usuario = json['id_usuario']
	usuario.id_moneda = json['id_moneda']
	usuario.balance = json['balance']

	usuario.update()

	return jsonify({'Usuario tiene Moneda': usuario.json() })

@app.route('/api/precio_moneda/<id_moneda>/<fecha>', methods=['PUT'])
def update_coin_price(id_moneda,fecha):
	price = Precio_moneda.query.filter_by(id_moneda=id_moneda,fecha=fecha).first()
	if price is None:
		return jsonify({'message': 'Account does not exists'}), 404

	json = request.get_json(force=True)
	if json.get('valor') is None:
		return jsonify({'message': 'El formato está mal'}), 400
	price.id_moneda = json['id_moneda']
	price.fecha = json['fecha']
	price.valor = json['valor']

	price.update()

	return jsonify({'Precio Moneda': price.json() })
#############################################################################

#############################################################################
# Endpoint para eliminar el usuario con id igual a <id>
@app.route('/api/usuario/<id>', methods=['DELETE'])
def delete_user(id):
	usuario = Usuario.query.filter_by(id=id).first()
	if usuario is None:
		return jsonify({'message': 'El usuario no existe'}), 404

	usuario.delete()

	return jsonify({'Usuario': usuario.json() })

@app.route('/api/pais/<cod_pais>', methods=['DELETE'])
def delete_country(cod_pais):
	pais = Pais.query.filter_by(cod_pais=cod_pais).first()
	if pais is None:
		return jsonify({'message': 'El pais no existe'}), 404

	pais.delete()

	return jsonify({'Pais': pais.json() })

@app.route('/api/cuenta_bancaria/<numero_cuenta>', methods=['DELETE'])
def delete_account(numero_cuenta):
	cuenta = Cuenta_bancaria.query.filter_by(numero_cuenta=numero_cuenta).first()
	if cuenta is None:
		return jsonify({'message': 'La cuenta no existe'}), 404

	cuenta.delete()

	return jsonify({'Cuenta Bancaria': cuenta.json() })

@app.route('/api/moneda/<id>', methods=['DELETE'])
def delete_coin(id):
	coin = Moneda.query.filter_by(id=id).first()
	if coin is None:
		return jsonify({'message': 'El usuario no existe'}), 404

	coin.delete()

	return jsonify({'Moneda': coin.json() })

@app.route('/api/usuario_tiene_moneda/<id_usuario>/<id_moneda>', methods=['DELETE'])
def delete_user_money(id_usuario,id_moneda):
	user = db.session.query(Usuario_tiene_moneda).filter(and_(Usuario_tiene_moneda.id_usuario == id_usuario , Usuario_tiene_moneda.id_moneda == id_moneda)).first()
	if user is None:
		return jsonify({'message': 'El usuario no existe'}), 404

	user.delete()

	return jsonify({'Usuario': user.json() })

@app.route('/api/precio_moneda/<id_moneda>/<fecha>', methods=['DELETE'])
def delete_coin_price(id_moneda,fecha):
	price = db.session.query(Precio_moneda).filter(and_(Precio_moneda.id_moneda == id_moneda , Precio_moneda.fecha == fecha)).first()
	if price is None:
		return jsonify({'message': 'El usuario no existe'}), 404

	price.delete()

	return jsonify({'Precio Moneda': price.json() })

#########################################################
#                      CONSULTAS						#
#########################################################
@app.route('/api/consultas/1/<year>', methods=['GET'])
def getFecha_RegistrobyYear(year):
	userinyear=[dict(Usuario) for Usuario in Usuario.getFecha_registrorequest(year=year).fetchall()]
	return jsonify({'Usuario': userinyear})

@app.route('/api/consultas/2/<selbalance>', methods=['GET'])
def get_account_by_balance(selbalance):
	cb_balance_sup=[dict(cuenta) for cuenta in Cuenta_bancaria.getByBalance(selbalance=selbalance).fetchall()]
	for cuenta in cb_balance_sup:
		for key,value in cuenta.items():
			if(key == 'balance'):
				cuenta[key]= str(value)
	return jsonify({'cuenta': cb_balance_sup})


@app.route('/api/consultas/3/<cod_pais>', methods=['GET'])
def get_user_by_country(cod_pais):
	userincountry=[dict(usuario) for usuario in Usuario.getUserByCountry(cod_pais=cod_pais).fetchall()]
	return jsonify({'usuario': userincountry})

@app.route('/api/consultas/4/<id_moneda>', methods=['GET'])
def get_max_of_coin(id_moneda):
	maxmoney=[dict(price) for price in Precio_moneda.getMaxByCoin(id_moneda=id_moneda).fetchall()]
	for price in maxmoney:
		for key,value in price.items():
			if(key == 'valor'):
				price[key]= str(value)
	return jsonify({'price': maxmoney})

@app.route('/api/consultas/5/<id_moneda>', methods=['GET'])
def get_moneda_abundante_user(id_moneda):
	cantidad=[dict(usuariocoin) for usuariocoin in Usuario_tiene_moneda.getMaxUserCoin(id_moneda=id_moneda).fetchall()]
	for usuariocoin in cantidad:
		for key,value in usuariocoin.items():
			if(key == 'balance'):
				usuariocoin[key]= str(value)
	return jsonify({'valor_maximo': cantidad})

@app.route('/api/consultas/6', methods=['GET'])
def top3():
	top=[dict(cant) for cant in Usuario_tiene_moneda.getTop3()]
	
	return jsonify({'TOP': top})


@app.route('/api/consultas/7/<year>/<month>', methods=['GET'])
def searchbymonth(year,month):
	monthsearch=[dict(num) for num in Precio_moneda.getmonthyear(year=year,month=month)]
	
	return jsonify({'search': monthsearch})

# Consulta 8

@app.route('/api/consultas/8/<id_usuario>', methods=['GET'])
def maxcoininuser(id_usuario):
	abundante=[dict(num) for num in Usuario_tiene_moneda.getMaxcoin(id_usuario=id_usuario)]
	for num in abundante:
		for key,value in num.items():
			if(key == 'Cantidad'):
				num[key]= str(value)
	return jsonify({'c8': abundante})


	
if __name__ == '__main__':
	app.run(debug=True)
