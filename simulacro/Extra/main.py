from flask import Flask
from flask import jsonify
from flask import request,redirect,url_for
from flask.templating import render_template
from flask_sqlalchemy import SQLAlchemy
import datetime
from config import config
from models import db, Usuario, Cuenta_bancaria, Pais, Moneda, Precio_moneda, Usuario_tiene_moneda
from flask import flash

def create_app(enviroment):
	app = Flask(__name__)
	app.secret_key = "Secret Key"
	app.config.from_object(enviroment)
	with app.app_context():
		db.init_app(app)
		db.create_all()
	return app

# Accedemos a la clase config del archivo config.py
enviroment = config['development']
app = create_app(enviroment)

###############################################################################
## Desarrollo Web (Html Direccionamientos)
###############################################################################
@app.route('/')
def Home():
	return render_template("index.html")
@app.route('/api/consultas')
def Consultas():
	return render_template("consultas.html")
@app.route('/404')
def Page_404():
	return redirect(url_for('404.html'))

@app.route('/api')
def api():
	all_user = Usuario.query.all()
	all_paises = Pais.query.all()
	all_cuentas_banciarias= Cuenta_bancaria.query.all()
	all_Usuario_tiene_moneda= Usuario_tiene_moneda.query.all()
	all_monedas=Moneda.query.all()
	all_precio_moneda=Precio_moneda.query.all()
	return render_template("api.html",Usuarios = all_user,Paises=all_paises,Cuentas_bancarias=all_cuentas_banciarias,Usuario_tiene_moneda=all_Usuario_tiene_moneda,Monedas=all_monedas,Precio_monedas=all_precio_moneda)

####################################################################
#      CRUD adaptandose
####################################################################


## USUARIOS
@app.route('/api/usuario/',methods= ['POST'])

def crear_usuario():
	if request.method == 'POST':
		nombre = request.form['nombre']
		apellido = request.form['apellido']
		correo = request.form['correo']
		contraseña = request.form['contraseña']
		pais = request.form['pais']
		my_usuario= Usuario(nombre=nombre,apellido=apellido,correo=correo,contraseña=contraseña,pais=pais,fecha_registro=datetime.datetime.now())
		db.session.add(my_usuario)
		db.session.commit()
		flash("Usuario Creado Satisfactoriamente")
		return redirect(url_for('api'))

@app.route('/api/usuario/update/', methods = ['POST','GET'])
def update_usuario(id):
	if request.method == 'POST':
		my_usuario = Usuario.query.get(request.form.get('id'))
		if id is None:
			return print('No ha sido encontrado el Usuario'), 404
		else:
			my_usuario.nombre = request.form['nombre']
			my_usuario.apellido = request.form['apellido']
			my_usuario.correo = request.form['correo']
			my_usuario.contraseña = request.form['contraseña']
			my_usuario.pais = request.form['pais']
			db.session.add(my_usuario)
			db.session.commit()
			flash("Datos Actualizados del Usuario")
		return redirect(url_for('api'))
 

@app.route('/api/usuario/delete/<id>/', methods = ['GET', 'POST'])
def delete(id):
	my_usuario = Usuario.query.get(id)
	db.session.delete(my_usuario)
	db.session.commit()
	flash("Datos borrados del usuario")
	return redirect(url_for('api'))
##################################################################################
@app.route('/api/pais',methods=['POST'])
def crear_pais():
	if request.method == 'POST':
		cod_pais = request.form['cod_pais']
		nombre_pais = request.form['nombre_pais']
		my_pais = Pais(cod_pais=cod_pais,nombre_pais=nombre_pais)
		db.session.add(my_pais)
		db.session.commit()
		
		flash("Creado el pais Correctamente :D")
		return redirect(url_for('api'))

@app.route('/api/pais/', methods = ['GET', 'POST'])
def update_pais(cod_pais):
	if request.method == 'POST':
		my_pais = Pais.query.get(cod_pais).format(cod_pais)
		my_pais.cod_pais = request.form['cod_pais']
		my_pais.nombre_pais = request.form['nombre_pais']
		db.session.commit()
		flash("Datos Actualizados")
		return redirect(url_for('api'))
app.add_url_rule('/api/pais/<cod_pais>','update_pais',update_pais)

@app.route('/api/pais/delete/<cod_pais>/', methods = ['GET', 'POST'])
def delete_pais(cod_pais):
	my_pais = Pais.query.get(cod_pais)
	db.session.delete(my_pais)
	db.session.commit()
	flash("Borrado el pais: ")
	return redirect(url_for('api'))
########################################################################################

@app.route('/api/cuenta_bancaria/create',methods=['POST'])
def crear_cuenta_bancaria():
	if request.method == 'POST':
		numero_cuenta = request.form['numero_cuenta']
		id_usuario = request.form['id_usuario']
		balance = request.form['balance']
		my_cuenta_bancaria = Cuenta_bancaria(numero_cuenta=numero_cuenta,id_usuario=id_usuario,balance=balance)
		db.session.add(my_cuenta_bancaria)
		db.session.commit()
		flash("Cuenta Bancaria creada con exito")

		return redirect(url_for('api'))
@app.route('/api/cuenta_bancaria/update/<numero_cuenta>',methods=['GET','POST'])
def update_c_b(numero_cuenta):
	if request.method == 'POST':
		my_c_b= Cuenta_bancaria.query.filter(numero_cuenta).first()
		my_c_b.numero_cuenta = request.form['numero_cuenta']
		my_c_b.id_usuario = request.form['id_usuario']
		my_c_b.balance = request.form['balance']
		db.session.commit()
		flash("Datos Actualizados de la cuenta bancaria ")
		return redirect(url_for('api'))
@app.route('/api/cuenta_bancaria/delete/<numero_cuenta>',methods=['GET','POST'])
def delete_cuenta_bancaria(numero_cuenta):
	my_c_b= Cuenta_bancaria.query.get(numero_cuenta)
	db.session.delete(my_c_b)
	db.session.commit()
	flash("Borrada la informacion")
	return redirect(url_for('api'))

# usuario_tiene_moneda

@app.route('/api/usuario_tiene_moneda/create',methods=['POST'])
def crear_usuario_tiene_moneda():
	if request.method == 'POST':
		id_usuario = request.form['id_usuario']
		id_moneda = request.form['id_moneda']
		balance = request.form['balance']
		my_utm = Cuenta_bancaria(id_usuario=id_usuario,id_moneda=id_moneda,balance=balance)
		db.session.add(my_utm)
		db.session.commit()
		flash("Usuario Tiene Moneda Creado")

		return redirect(url_for('api'))

@app.route('/api/usuario_tiene_moneda/update/',methods=['GET','POST'])
def update_usuario_tiene_moneda():
	if request.method == 'POST':
		my_utm= Cuenta_bancaria.query.filter(id_usuario).first()
		my_utm.id_usuario = request.form['id_usuario']
		my_utm.id_moneda = request.form['id_moneda']
		my_utm.balance = request.form['balance']
		db.session.commit()
		flash("Datos Actualizados")
		return redirect(url_for('api'))

@app.route('/api/usuario_tiene_moneda/delete/<id_usuario>',methods=['GET','POST'])
def delete_usuario_tiene_moneda(id_usuario):
	my_utm= Usuario_tiene_moneda.query.get(id_usuario)
	db.session.delete(my_utm)
	db.session.commit()
	flash("Borrada la informacion")
	return redirect(url_for('api'))

# moneda

@app.route('/api/moneda/create',methods=['POST'])
def crear_moneda():
	if request.method == 'POST':
		id = request.form['id']
		sigla = request.form['sigla']
		nombre = request.form['nombre']
		my_moneda = Moneda(id=id,sigla=sigla,nombre=nombre)
		db.session.add(my_moneda)
		db.session.commit()
		flash("Moneda creada con exito")

		return redirect(url_for('api'))
@app.route('/api/moneda/update/',methods=['GET','POST'])
def update_moneda():
	if request.method == 'POST':
		my_moneda= Moneda.query.filter(id).first()
		my_moneda.id = request.form['id']
		my_moneda.sigla = request.form['sigla']
		my_moneda.nombre = request.form['nombre']
		db.session.commit()
		flash("Datos Actualizados")
		return redirect(url_for('api'))
@app.route('/api/moneda/delete/<id>',methods=['GET','POST'])
def delete_moneda(id):
	my_moneda= Moneda.query.get(id)
	db.session.delete(my_moneda)
	db.session.commit()
	flash("Borrada la informacion")
	return redirect(url_for('api'))

# precio moneda

@app.route('/api/precio_moneda/create',methods=['POST'])
def crear_precio_moneda():
	if request.method == 'POST':
		id_moneda = request.form['id_moneda']
		valor = request.form['valor']
		my_pm = Precio_moneda(id_moneda=id_moneda,fecha=datetime.datetime.now(),valor=valor)
		db.session.add(my_pm)
		db.session.commit()
		flash("Precio Moneda creado con exito")

		return redirect(url_for('api'))
@app.route('/api/precio_moneda/update/',methods=['GET','POST'])
def update_precio_moneda():
	if request.method == 'POST':
		my_pm= Precio_moneda.query.filter(id_moneda).first()
		my_pm.id_moneda = request.form['id_moneda']
		my_pm.fecha = request.form['fecha']
		my_pm.valor = request.form['valor']
		db.session.commit()
		flash("Datos Actualizados")
		return redirect(url_for('api'))
@app.route('/api/precio_moneda/delete/<id_moneda>',methods=['PUT'])
def delete_precio_moneda(id_moneda):
	my_pm= Precio_moneda.query.filter_by(id_moneda=id_moneda).first() 
	if my_pm is None:
		return flash('No existe la data'), 404
	try: 
		db.session.delete(my_pm)
		db.session.commit()
		flash("Borrada la informacion")
		return redirect(url_for('api'))
	except:
		return False

	
if __name__ == '__main__':
	app.run(debug=True)
