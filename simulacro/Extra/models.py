from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text
from sqlalchemy.orm import backref, relationship
from sqlalchemy import exc
import sys

db = SQLAlchemy()

class Usuario(db.Model):
	__tablename__ = 'usuario'
	id = db.Column(db.Integer,  primary_key=True)
	nombre = db.Column(db.String(50), nullable=False)
	apellido = db.Column(db.String(50), nullable=True)
	correo = db.Column(db.String(50), nullable=False)
	contraseña = db.Column(db.String(50), nullable=False)
	pais = db.Column(db.Integer, db.ForeignKey('pais.cod_pais'))
	fecha_registro = db.Column(db.DateTime(), nullable=False, default=db.func.current_timestamp())
	
	cuenta_bancaria = db.relationship('Cuenta_bancaria', cascade="all,delete", backref="usuario")
	usuario_tiene_moneda = db.relationship('Usuario_tiene_moneda', cascade="all,delete", backref="usuario")

	@classmethod
	def create(cls, _nombre, _apellido, _correo, _contraseña, _pais):
		_usuario = Usuario(nombre=_nombre, apellido=_apellido, correo=_correo, contraseña=_contraseña, pais=_pais)
		return _usuario.save()

	def save(self):
		try:
			db.session.add(self)
			db.session.commit()
			return self
		except exc.IntegrityError:
			print("Oops!", sys.exc_info(), "occurred.")
			return False

	def json(self):
		return {
			'id': self.id,
			'nombre': self.nombre,
			'apellido': self.apellido,
			'correo': self.correo,
			'contraseña': self.contraseña,
			'pais': self.pais,
			'fecha_registro': self.fecha_registro
		}

	def update(self):
		self.save()

	def delete(self):
		try:
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False

class Pais(db.Model):
	__tablename__ = 'pais'
	cod_pais = db.Column(db.Integer, primary_key=True)
	nombre_pais = db.Column(db.String(45), nullable=False)

	@classmethod
	def create(cls, _nombre):
		_pais = Pais(nombre=_nombre)
		return _pais.save()

	def save(self):
		try:
			db.session.add(self)
			db.session.commit()
			return self
		except exc.IntegrityError:
			print("Oops!", sys.exc_info(), "occurred.")
			return False

	def json(self):
		return {
			'cod_pais': self.cod_pais,
			'nombre_pais': self.nombre_pais,
		}

	def update(self):
		self.save()

	def delete(self):
		try: 
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False

class Cuenta_bancaria(db.Model):
	__tablename__ = 'cuenta_bancaria'
	numero_cuenta = db.Column(db.Integer, primary_key=True)
	id_usuario = db.Column(db.Integer, db.ForeignKey('usuario.id'))
	balance = db.Column(db.Float, nullable=False)

	@classmethod
	def create(cls, _id_usuario, _balance):
		_cuenta = Cuenta_bancaria(id_usuario=_id_usuario, balance=_balance)
		return _cuenta.save()

	def save(self):
		try:
			db.session.add(self)
			db.session.commit()
			return self
		except exc.IntegrityError:
			print("Oops!", sys.exc_info(), "occurred.")
			return False

	def json(self):
		return {
			'numero_cuenta': self.numero_cuenta,
			'id_usuario': self.id_usuario,
			'balance': self.balance
		}

	def update(self):
		self.save()

	def delete(self):
		try:
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False

class Moneda(db.Model):
	__tablename__ = 'moneda'
	id = db.Column(db.Integer, primary_key=True)

	sigla = db.Column(db.String(10), nullable=False)
	nombre = db.Column(db.String(80), nullable=False)

	usuario_tiene_moneda = db.relationship('Usuario_tiene_moneda', cascade="all,delete", backref="moneda")
	precio_moneda = db.relationship('Precio_moneda', cascade="all,delete", backref="moneda")

	@classmethod
	def create(cls, _sigla, _nombre):
		_moneda = Moneda(sigla=_sigla, nombre=_nombre)
		return _moneda.save()

	def save(self):
		try:
			db.session.add(self)
			db.session.commit()
			return self
		except exc.IntegrityError:
			print("Oops!", sys.exc_info(), "occurred.")
			return False

	def json(self):
		return {
			'id': self.id,
			'sigla': self.sigla,
			'nombre': self.nombre
		}

	def update(self):
		self.save()

	def delete(self):
		try:
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False

class Usuario_tiene_moneda(db.Model):
	__tablename__ = 'usuario_tiene_moneda'
	id_usuario = db.Column(db.Integer, db.ForeignKey('usuario.id'), primary_key=True)
	id_moneda = db.Column(db.Integer, db.ForeignKey('moneda.id'), primary_key=True)
	balance = db.Column(db.Float, nullable=False)

	@classmethod
	def create(cls, _id_usuario, _id_moneda, _balance):
		_usuario = Usuario_tiene_moneda(id_usuario=_id_usuario, id_moneda=_id_moneda, balance=_balance)
		return _usuario.save()

	def save(self):
		try:
			db.session.add(self)
			db.session.commit()
			return self
		except exc.IntegrityError:
			print("Oops!", sys.exc_info(), "occurred.")
			return False

	def json(self):
		return {
			'id_usuario': self.id_usuario,
			'id_moneda': self.id_moneda,
			'balance': self.balance
		}

	def update(self):
		self.save()

	def delete(self):
		try:
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False

class Precio_moneda(db.Model):
	__tablename__ = 'precio_moneda'
	id_moneda = db.Column(db.Integer, db.ForeignKey('moneda.id'), primary_key=True)
	fecha = db.Column(db.DateTime(), primary_key=True, default=db.func.current_timestamp())
	valor = db.Column(db.Float, nullable=False)

	@classmethod
	def create(cls, _id_moneda, _valor):
		_usuario = Precio_moneda(id_moneda=_id_moneda, valor=_valor)
		return _usuario.save()

	def save(self):
		try:
			db.session.add(self)
			db.session.commit()
			return self
		except exc.IntegrityError:
			print("Oops!", sys.exc_info(), "occurred.")
			return False

	def json(self):
		return {
			'id_moneda': self.id_moneda,
			'fecha': self.fecha,
			'valor': self.valor
		}
		

	def update(self):
		self.save()

	def delete(self):
		try:
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False
			