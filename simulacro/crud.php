<?php

//       READ            [USUARIOS]
$ch= curl_init();

curl_setopt($ch,CURLOPT_URL,'http://localhost:5000/api/usuario');
curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

$response = curl_exec($ch);

if (curl_error($ch)){
    echo curl_error($ch);
}
else{ 
    $decoded= json_decode($response,true);
}
curl_close($ch);
$contadorusuarios= count($decoded['usuarios']);

// READ PAIS
$ch01= curl_init();

curl_setopt($ch01,CURLOPT_URL,'http://localhost:5000/api/pais');
curl_setopt($ch01,CURLOPT_RETURNTRANSFER,true);

$response1 = curl_exec($ch01);

if (curl_error($ch01)){
    echo curl_error($ch1);
}
else{ 
    $decoded1= json_decode($response1,true);
    
}

curl_close($ch01);

$contadorpaises = count($decoded1['Paises']);

//   READ   Cuenta Bancaria

$ch2= curl_init();

curl_setopt($ch2,CURLOPT_URL,'http://localhost:5000/api/cuenta_bancaria');
curl_setopt($ch2,CURLOPT_RETURNTRANSFER,true);

$response2 = curl_exec($ch2);

if (curl_error($ch2)){
    echo curl_error($ch2);
}
else{ 
    $decoded2= json_decode($response2,true);
    
}

curl_close($ch2);
$contadorc_b= count($decoded2['Cuentas Bancarias']);

// READ Usuario Tiene Moneda

$ch3= curl_init();

curl_setopt($ch3,CURLOPT_URL,'http://localhost:5000/api/usuario_tiene_moneda');
curl_setopt($ch3,CURLOPT_RETURNTRANSFER,true);

$response3 = curl_exec($ch3);

if (curl_error($ch3)){
    echo curl_error($ch3);
}
else{ 
    $decoded3= json_decode($response3,true);
    
}
curl_close($ch3);

$contadorutm= count($decoded3['usuarios con moneda']);

//   READ   Moneda
$ch4= curl_init();
curl_setopt($ch4,CURLOPT_URL,'http://localhost:5000/api/moneda');
curl_setopt($ch4,CURLOPT_RETURNTRANSFER,true);
$response4 = curl_exec($ch4);
if (curl_error($ch4)){
    echo curl_error($ch4);
}
else{ 
    $decoded4= json_decode($response4,true);
    
}
curl_close($ch4);
$contadormoneda= count($decoded4['Monedas']);

//   READ  PRECIO Moneda
$ch5= curl_init();
curl_setopt($ch5,CURLOPT_URL,'http://localhost:5000/api/precio_moneda');
curl_setopt($ch5,CURLOPT_RETURNTRANSFER,true);
$response5 = curl_exec($ch5);
if (curl_error($ch5)){
    echo curl_error($ch5);
}
else{ 
    $decoded5= json_decode($response5,true);
}
curl_close($ch5);
$contadorpmoneda= count($decoded5['Precios de monedas']);


//---------------------Funcion para POST, PUT

function callAPI($method, $url, $data){
    $curl = curl_init();
    switch ($method){
       case "POST":
          curl_setopt($curl, CURLOPT_POST, 1);
          if ($data)
             curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
          break;
       case "PUT":
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
          if ($data)
             curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
          break;
        case "DELETE":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
            if($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
       default:
          if ($data)
             $url = sprintf("%s?%s", $url, http_build_query($data));
    }
    // OPTIONS:
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    // EXECUTE:
    $result = curl_exec($curl);
    if(!$result){die("Connection Failure");}
    curl_close($curl);
    return $result;
 }
 //_____________________________________
//       CREATE           [USUARIO]


if(isset($_POST['create'])){
    $id= $contadorusuarios + 1;
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $correo = $_POST['correo'];
    $contraseña = md5($_POST['contraseña']);
    $pais = $_POST['pais'];
    $fecha_registro= date("Y-m-d H:i:s");
    $createuser_array = array(
        "id" =>  $id,
        "nombre"=> $nombre,
        "apellido"=> $apellido,
        "correo"=> $correo,
        "contraseña"=> $contraseña,
        "pais"=> $pais,
        "fecha_registro"=> $fecha_registro
    );
    $make_user = callAPI('POST','http://localhost:5000/api/usuario',json_encode($createuser_array));
    header('location: crud.html');
}


//      CREATE                 [PAIS]

if(isset($_POST['createpais'])){
    $cod_pais= $_POST['cod_pais'];
    $nombre_pais = $_POST['nombre_pais'];
    $createpais_array = array(
        "cod_pais" =>  $cod_pais,
        "nombre_pais"=> $nombre_pais
    );
    $make_pais = callAPI('POST','http://localhost:5000/api/pais',json_encode($createpais_array));
    if (!$make_pais){
        echo 'No se ha podido crear el pais';
        header('location: crud.html');
    }
    header('location: crud.html');
}

//      CREATE                 [Cuenta Bancaria]

if(isset($_POST['create_cb'])){
    $numero_cuenta= $_POST['numero_cuenta'];
    $id_usuario = $_POST['id_usuario'];
    $balance = $_POST['balance'];
    $createcb_array = array(
        "numero_cuenta" =>  $numero_cuenta,
        "id_usuario"=> $id_usuario,
        "balance" => $balance
    );
    $make_cb = callAPI('POST','http://localhost:5000/api/cuenta_bancaria',json_encode($createcb_array));
    header('location: crud.html');
}
//      CREATE                 [Usuario Tiene Moneda]

if(isset($_POST['create_utm'])){
    $id_usuario= $_POST['id_usuario'];
    $id_moneda = $_POST['id_moneda'];
    $balance = $_POST['balance'];
    $createutm_array = array(
        "id_usuario" =>  $id_usuario,
        "id_moneda"=> $id_moneda,
        "balance" => $balance
    );
    $make_utm = callAPI('POST','http://localhost:5000/api/usuario_tiene_moneda',json_encode($createutm_array));
    header('location: crud.html');
}

//      CREATE                 [Moneda]

if(isset($_POST['create_moneda'])){
    $id= $_POST['id'];
    $sigla = $_POST['sigla'];
    $nombre = $_POST['nombre'];
    $createmoneda_array = array(
        "id" =>  $id,
        "sigla"=> $sigla,
        "nombre" => $nombre
    );
    $make_moneda = callAPI('POST','http://localhost:5000/api/moneda',json_encode($createmoneda_array));
    header('location: crud.html');
}
//      CREATE                 [Precio Moneda]

if(isset($_POST['create_pm'])){
    $id_moneda= $_POST['id_moneda'];
    $fecha = date("Y-m-d H:i:s");
    $valor = $_POST['valor'];
    $createpm_array = array(
        "id_moneda" =>  $id_moneda,
        "fecha"=> $fecha,
        "valor" => $valor
    );
    $make_pm = callAPI('POST','http://localhost:5000/api/precio_moneda',json_encode($createpm_array));
    header('location: crud.html');
}

$select_user = 'http://localhost:5000/api/usuario';
$select_pais= 'http://localhost:5000/api/pais';
$select_cb = 'http://localhost:5000/api/cuenta_bancaria';
$select_utm= 'http://localhost:5000/api/usuario_tiene_moneda';
$select_moneda = 'http://localhost:5000/api/moneda';
$select_pm= 'http://localhost:5000/api/precio_moneda';
//       DELETE             USUARIO

if(isset($_POST['delete_user'])){
    $url_user=$_POST['url_user'];
    $delete_userid = callAPI('DELETE',$url_user,false);
    header('location: crud.html');
}

//      DELETE             PAIS

if(isset($_POST['delete_pais'])){
    $url_paisid = $_POST['url_paisdelete'];
    $delete_paisid = callAPI('DELETE',$url_paisid,false);
    header('location: crud.html');
}


//      DELETE             Cuenta Bancaria

if(isset($_POST['delete_cb'])){
    $url_cb = $_POST['url_cb'];
    $delete_cb = callAPI('DELETE',$url_cb,false);
    header('location: crud.html');
}


//      DELETE             Usuario Tiene Moneda

if(isset($_POST['delete_utm'])){
    $url_utm = $_POST['url_utm'];
    $delete_utm = callAPI('DELETE',$url_utm,false);
    header('location: crud.html');
}


//      DELETE             Moneda

if(isset($_POST['delete_moneda'])){
    $url_moneda = $_POST['url_moneda'];
    $delete_moneda = callAPI('DELETE',$url_moneda,false);
    header('location: crud.html');
}


//      DELETE             Precio Moneda

//Revisar

if(isset($_POST['delete_pm'])){
    $url_pm = $_POST['url_pm'];
    $delete_pm = callAPI('DELETE',$url_pm,false);
    echo $delete_pm;
}

?>
