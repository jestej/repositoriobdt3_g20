# Readme Tarea 3 Base de Datos 


## Integrantes:

<table style="width:100%">
    <tr>
        <th>Nombre</th>
        <th>Rol USM</th>
    </tr>
    <tr>
        <th>Alejandro Caceres</th>
        <th>201930032-</th>
    </tr>
    <tr>
        <th>Nicolas Capetillo</th>
        <th>201930049-0</th>
    </tr>
    <tr>
        <th>Vicente Tejos</th>
        <th>201930017-2</th>
    </tr>    
</table>      <br />


-------

<h1>Buenas, <b>Ayudantes</b></h1>


<p>Reciban un cordial saludo por parte de nuestro grupo, esperamos que tengan una buena estadia en la revision de la Tarea Final.</p>
<br/>
<p> Esta tarea ha tomado 91 [hr] de desarrollo, la cual ha sido desarrollada en la computadora de uno de nuestros integrantes, ese es el motivo de que uno solo ha realizado commits. Hemos desarrollado la tarea atraves de discord y con comparticion de pantalla. 

Sin mayor dilatación los invitamos a leer las siguientes lineas donde se detallan las suposiciones y que decisiones se han tomado para solucionar los diversos problemas.

</p>

-------------

# Cryptomonedas: P-Coin III Final

<p>En esta actividad se ha solicitado realizar una api en Flask "Python", esta va actuar de intermediario entre el servidor local y la pagina web realizando las consultas que se han solicitado.</p>

<h1>Credenciales de Ingreso</h1>

correo: ayudante.inf@usm.cl

pass: admin

Si no funciona la anterior existe esta

correo: vicente.tejos@usm.cl

pass: 123



<br/>
<br/>

# Explicacion del desarrollo de la tarea


<br/>
<br/>
<br/>
<h1>Inicios al Consumir la API</h1>
<div>
<p>En un inicio después de haber desarrollado la API, buscamos por la web la manera de trabajar con la API en esos caminos de google se llego a dar trabajar los direccionamientos de la Web y todo en general a traves de Python por FLASK. Despues de trabajar 4 dias con el CRUD en Python, uno de nuestros integrantes le consulta al ayudante como solucionar un problema. Lo cual el ayudante impactado informa al integrante que no debia hacerse de esa manera y que se debia consumir la API por PHP o JavaScript. 

*Por cierto* lo desarrollado en python se encuentra en simulacro/Extra

Se intenta realizar con JavaScript, pero los estudiantes aun estan chiquitos para ello, por lo cual se trabaja con PHP con curl.
</p>
</div>

<br/>

---
<h1>Forma de abarcar la problemática</h1>
---

<p>Para desarrollar las interfaces se trabaja con curl para consumir la API donde se encuentran todas las consultas que se le realizan a la base de datos. En /simulacro/crud.php se encuentra todas consultas a la API, en las primeras lineas se realiza la lectura de los datos de cada una de las tablas, las cuales después son recorridas con un for en sus respectivos campos de cada tabla.  </p>

<p> Posteriormente se trabaja con la creacion, actualizacion y borrar la data, donde se presentan ciertas complicaciones. </p>

<p>Una de las complicaciones es que la data no se puede crear porque faltan ciertos parametros en el models.py, para solucionar esta problematica se le agregan los parametros restantes y funciona de manera adecuada para poder abarcar con todos los requisitos.</p>

<p>Aunque una complicacion muy grande ha sido poder actualizar y borrar la información de la tabla precio_moneda. Debido que este depende de la fecha y  id_moneda, necesitamos ambos parametros para poder borrar y editar la informacion de la tabla.</p>

<p>Ahora por el otro lado las consultas en el caso de las consultas han existido complicaciones con los decimales presentes que no permiten obtener la informacion de la base dato, por lo cual se desarrollo recorrer el diccionario y cambiar la data a string y quitar la complicacion de que no se pueda obtener. (Como se puede dimensionar en el main.py linea 321 donde se presenta el primer Error y donde se trabaja con decimales).</p>


# Distribucion de los archivos 

<p>Todo lo relacionado con la Tarea se encuentra en el directorio /simulacro</p>

<p>El Directorio Simulacro contiene</p>
<ul>
    <li>Extra</li>
    <p>Desarrollo parte de la tarea que se hizo por error en python siendo fuente conexion API y servidor WEB</p>
    <li>Read</li>
    <p>Directorio donde se encuentra todas las implementaciones relacionadas a la lectura de cada elemento de cada tabla. Ejemplo: Presiono Ver en la tabla Usuario a un usuario Random y me entrega la data relacionada al mismo.</p>
    <li>request</li>
    <p>Directorio donde se encuentra las consultas de que se debian implementar en la tarea 3.</p>
    <li>update</li>
    <p>Directorio relacionado a implementacion de actualización(Modificación) de cada elemento de las distintas tablas</p>
    <li>crud.html</li>
    <p>Todo el Crud tiene como base principal esta pagina, aqui se muestra los datos de cada tabla</p>
    <li>crud.php</li>
    <p>En este archivo se consume la API relacionada al CRUD. Exceptuando en el caso del update</p>
    <li>simulacion.html</li>
    <p>Página principal que se redirecciona desde la pagina realizada en la Tarea 2, obviamente para poder ver la simulacion.html el usuario debe de ser admin e ingresar a la sección Simulación en el navbar</p>
</ul>



# Informacion relacionada al dump

<p>En la Tarea Anteior no guardamos el Dump relacionado a las modificaciones que se le realizaron a la tabla, por ende la calificación se vio reducida. Por lo mismo en la actualidad se actualizo el Dump del la carpeta raiz del proyecto para no tener complicaciones y tambien el dump relacionado a la tarea 3 que se encuentra en la carpeta /Simulacro.</p>


*Pd. En caso de haber complicaciones informar por favor y se envia el dump correspondiente como lo hicieron otros ayudantes con otros grupos, gracias de ante mano.*

-------

*Contactos*

*Discord: Jestej#9086*

*Correo: vicente.tejos@usm.cl*

-----

# Suposiciones realizadas al trabajar 

<p> Para leer la data de las tablas consumo la api solicitando de los datos que se encuentran almacenados en la base de datos y despues recorro el arreglo del json. Y muestro la información accediendo al diccionario.</p>
<br />
<p>Para mostrar la data en el read se le paso en un formulario con paramatros de tipo hidden y estos son pasados y se puede mostrar la data que estoy leyendo. Al igual que para el update se aplica el mismo procedimiento para saber como estaban la informacion en la tabla y para poder modificarla.</p>

<br />
<br />

<p> Para las consultas todo se encuentra en el request</p>


--------------------------
