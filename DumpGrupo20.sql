PGDMP                          y            Tarea2BD    12.6    12.6 '    :           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            ;           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            <           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            =           1262    16868    Tarea2BD    DATABASE     �   CREATE DATABASE "Tarea2BD" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Chile.1252' LC_CTYPE = 'Spanish_Chile.1252';
    DROP DATABASE "Tarea2BD";
                postgres    false                        3079    16869 	   adminpack 	   EXTENSION     A   CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;
    DROP EXTENSION adminpack;
                   false            >           0    0    EXTENSION adminpack    COMMENT     M   COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';
                        false    1            �            1259    16878    cuenta_bancaria    TABLE     �   CREATE TABLE public.cuenta_bancaria (
    numero_cuenta integer NOT NULL,
    id_usuario integer NOT NULL,
    balance numeric NOT NULL
);
 #   DROP TABLE public.cuenta_bancaria;
       public         heap    postgres    false            �            1259    16884 
   foo_id_seq    SEQUENCE     s   CREATE SEQUENCE public.foo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.foo_id_seq;
       public          postgres    false            �            1259    16886    id_seq    SEQUENCE     o   CREATE SEQUENCE public.id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE public.id_seq;
       public          postgres    false            �            1259    16888    moneda    TABLE     �   CREATE TABLE public.moneda (
    id integer NOT NULL,
    sigla character varying(10) NOT NULL,
    nombre character varying(80) NOT NULL
);
    DROP TABLE public.moneda;
       public         heap    postgres    false            �            1259    16891    pais    TABLE     l   CREATE TABLE public.pais (
    cod_pais integer NOT NULL,
    nombre_pais character varying(45) NOT NULL
);
    DROP TABLE public.pais;
       public         heap    postgres    false            �            1259    16894    precio_moneda    TABLE     �   CREATE TABLE public.precio_moneda (
    id_moneda integer NOT NULL,
    fecha timestamp without time zone NOT NULL,
    valor numeric NOT NULL
);
 !   DROP TABLE public.precio_moneda;
       public         heap    postgres    false            �            1259    16900    usuario    TABLE     z  CREATE TABLE public.usuario (
    id integer DEFAULT nextval('public.id_seq'::regclass) NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(50),
    correo character varying(50) NOT NULL,
    "contraseña" character varying(50) NOT NULL,
    pais integer NOT NULL,
    fecha_registro timestamp without time zone NOT NULL,
    es_admin boolean
);
    DROP TABLE public.usuario;
       public         heap    postgres    false    205            �            1259    16904    usuario_id_seq    SEQUENCE     w   CREATE SEQUENCE public.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.usuario_id_seq;
       public          postgres    false            �            1259    16906    usuario_tiene_moneda    TABLE     �   CREATE TABLE public.usuario_tiene_moneda (
    id_usuario integer NOT NULL,
    id_moneda integer NOT NULL,
    balance numeric NOT NULL
);
 (   DROP TABLE public.usuario_tiene_moneda;
       public         heap    postgres    false            /          0    16878    cuenta_bancaria 
   TABLE DATA           M   COPY public.cuenta_bancaria (numero_cuenta, id_usuario, balance) FROM stdin;
    public          postgres    false    203   �.       2          0    16888    moneda 
   TABLE DATA           3   COPY public.moneda (id, sigla, nombre) FROM stdin;
    public          postgres    false    206   �/       3          0    16891    pais 
   TABLE DATA           5   COPY public.pais (cod_pais, nombre_pais) FROM stdin;
    public          postgres    false    207   U0       4          0    16894    precio_moneda 
   TABLE DATA           @   COPY public.precio_moneda (id_moneda, fecha, valor) FROM stdin;
    public          postgres    false    208   �0       5          0    16900    usuario 
   TABLE DATA           n   COPY public.usuario (id, nombre, apellido, correo, "contraseña", pais, fecha_registro, es_admin) FROM stdin;
    public          postgres    false    209   8       7          0    16906    usuario_tiene_moneda 
   TABLE DATA           N   COPY public.usuario_tiene_moneda (id_usuario, id_moneda, balance) FROM stdin;
    public          postgres    false    211   <       ?           0    0 
   foo_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.foo_id_seq', 1, false);
          public          postgres    false    204            @           0    0    id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('public.id_seq', 29, true);
          public          postgres    false    205            A           0    0    usuario_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.usuario_id_seq', 1, false);
          public          postgres    false    210            �
           2606    16913 $   cuenta_bancaria cuenta_bancaria_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_pkey PRIMARY KEY (numero_cuenta);
 N   ALTER TABLE ONLY public.cuenta_bancaria DROP CONSTRAINT cuenta_bancaria_pkey;
       public            postgres    false    203            �
           2606    16915    moneda moneda_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.moneda
    ADD CONSTRAINT moneda_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.moneda DROP CONSTRAINT moneda_pkey;
       public            postgres    false    206            �
           2606    16917    pais pais_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (cod_pais);
 8   ALTER TABLE ONLY public.pais DROP CONSTRAINT pais_pkey;
       public            postgres    false    207            �
           2606    16919     precio_moneda precio_moneda_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.precio_moneda
    ADD CONSTRAINT precio_moneda_pkey PRIMARY KEY (id_moneda, fecha);
 J   ALTER TABLE ONLY public.precio_moneda DROP CONSTRAINT precio_moneda_pkey;
       public            postgres    false    208    208            �
           2606    16921    usuario usuario_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pkey;
       public            postgres    false    209            �
           2606    16923 .   usuario_tiene_moneda usuario_tiene_moneda_pkey 
   CONSTRAINT        ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_pkey PRIMARY KEY (id_usuario, id_moneda);
 X   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_pkey;
       public            postgres    false    211    211            �
           2606    16924 /   cuenta_bancaria cuenta_bancaria_id_usuario_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 Y   ALTER TABLE ONLY public.cuenta_bancaria DROP CONSTRAINT cuenta_bancaria_id_usuario_fkey;
       public          postgres    false    209    203    2725            �
           2606    16929 0   cuenta_bancaria cuenta_bancaria_id_usuario_fkey1    FK CONSTRAINT     �   ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_id_usuario_fkey1 FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 Z   ALTER TABLE ONLY public.cuenta_bancaria DROP CONSTRAINT cuenta_bancaria_id_usuario_fkey1;
       public          postgres    false    203    209    2725            �
           2606    16934    cuenta_bancaria id_usuario    FK CONSTRAINT     �   ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES public.usuario(id) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY public.cuenta_bancaria DROP CONSTRAINT id_usuario;
       public          postgres    false    203    209    2725            �
           2606    16939    usuario_tiene_moneda id_usuario    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES public.usuario(id) ON UPDATE CASCADE ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT id_usuario;
       public          postgres    false    2725    209    211            �
           2606    16944 *   precio_moneda precio_moneda_id_moneda_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.precio_moneda
    ADD CONSTRAINT precio_moneda_id_moneda_fkey FOREIGN KEY (id_moneda) REFERENCES public.moneda(id);
 T   ALTER TABLE ONLY public.precio_moneda DROP CONSTRAINT precio_moneda_id_moneda_fkey;
       public          postgres    false    208    2719    206            �
           2606    16949    usuario usuario_pais_fkey    FK CONSTRAINT     z   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pais_fkey FOREIGN KEY (pais) REFERENCES public.pais(cod_pais);
 C   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pais_fkey;
       public          postgres    false    2721    207    209            �
           2606    16954 8   usuario_tiene_moneda usuario_tiene_moneda_id_moneda_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_id_moneda_fkey FOREIGN KEY (id_moneda) REFERENCES public.moneda(id);
 b   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_id_moneda_fkey;
       public          postgres    false    2719    206    211            �
           2606    16959 9   usuario_tiene_moneda usuario_tiene_moneda_id_usuario_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 c   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_id_usuario_fkey;
       public          postgres    false    2725    209    211            �
           2606    16964 :   usuario_tiene_moneda usuario_tiene_moneda_id_usuario_fkey1    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_id_usuario_fkey1 FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 d   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_id_usuario_fkey1;
       public          postgres    false    2725    209    211            /   �   x��ɑE!�V0.�۹L�q��/.jI����gpq)�������t�k5��oZs��a=��Pg]i�ڢ\7p2ݭ{�{��0��-4��gZ9��>X&�ո�>pצ#\r/�6<!�Sjt؏|RF���|:?�\}���غ�BS?cׅ�G�kzOaD[���K�q�C69��ړF��:	�4�ܴ<�mK�O��P�}���|��G$�|u
���Dp      2   �   x�-��
�@���>�O�[g��N`�F�b:#z}��ڝ��#�
��:8O���T�<���f6�^����B���{<
ma���fM��B�m�!2S�~�#�<C���e:!Q�?�:C��l�Q\�²��T�D��2�kED!�2�      3   t   x���1���p��Ox�. �DF��V�l�>'tC-4�F��`��U�p�����Y�!��KS�0�EBi���=�[S�coK���#.9O�Fq!&7��3��Y�6׏�"�i�#E      4   (  x�]X]�#�
{��*��D�o���צ�@F��H�SE�1��?m��m��y5�t\��aC�[��D��Υ������y�v	 ��%��{5ܲ/���iD�������8e����j��pw�y */Y�h�b��o����U=�s�Y���ٯ6/�W;yH��9/�z�OH�"{�`(�k?���<��߫���Ң���x[�E(�D,w΀����z᠖�E�Z �� 'Z��gb�ܢ�-���� y|P���x�l��c��\�T�,���]vp�{H�zɼ�]5\��O���������	������2����.RN�3+&W�P�R�.ᗈ��@�-$�]IYҞ'�\�L-���y2 �/R��0[��h�U��lK:���A��/y�P+��`�t�IpQpe_d��P#�z^DR����S:ؿD����g,G?�@�C���A��*���Ս����Ĳ���.t!/�S�B��~���d��ڞ���9��=\E�X���F��@�D��r\��.;-'�2Kz�� �FD��3�B�!$Tc���f��&җ� ��E͞����Ӡ��E���V\O[:5����Nn�)bٗ��3B��Ju�YF�^ w�͛(B� E�@�f�EZ8�f����9��D�.cYr�Ѓ,H诧���
1�\�Ŗ�S�gN8|{����B���zK.4�'缊fF�䰡d�E�|��trح<(Å��A9��!���8}���\":�Eh �����*��w+�y�S�����`�Re��r7�d,���XP�n���Q���`]+ukb��3g�SJŋ��r
�?5�2R���ȭ����RTz�tNN�ᮩH��&��\�s��B]v�h�h�rʦY�)Z��H��Y慩�]����V ����4�K�t.	a�mI�P�nToy�2����?�nd]!�j?��9��2�D�
9_�Ls#n�F�7�{�J�� �eLCM�=*�;D�0ҵ� �L�K4IK�֭ዐ�:�t7/R���4����$�y�OJֹ�Ao�H��$���q��$<�@�6N�cO�n��]Ȭ��k�=+�[�Ɋ#h�;ӟzy\�gN�����ٛ���E�0�̭M�-�n��EqQ�3%u8D
�T���8��&=�͕��΃��S�P��6��ӝn5�9�0�
���P\/w	�+��=l9u�ٌ����{ǋ�Ҙ���T����7�tIt���R/�S㔴[h���c�T�G犜��ي5�%�Eұ̳**O�_��-Mj+���A����}
�;��'��$�W`n��q�Y)�2M3q����\7����h�3в��q���~^�.�:���)|���e�]N�.��r���'[֏*��g�p����ren)D�/!J�me���!��K���?��Υg������B��,d��93}�N�ո$�G�>������M��|������.B��-�n��i�v=+���uU�9�|��`�w��

��
�s~���%��!�[�����>Y�ww��9ZO+�(�|s-֝+_@���Ve�V�(vR�=-}��ʨ�'+�;�Ö
�{����\�9O���&�R	�X��\g���i\��:�?�Ba�$���N��<�����1�;������}�ڟ�L�qJU�z|.f�R9ir����c��FZ��H'5+fxȫ���!so�JiΆ샛�������|��ϖ=�	j$��e�PY0���WN���֜����$�s���cK-6-J����m�Zl���������]\      5   �  x�]U�n�F|n�� s�s�I�kq�,�A�!5����%�5���RvH�5K�]5�R߶���i硇*@r����2���Dꡃ�,��Zh�h8e*�,��'Jo�<�H�;�c�ݝ!��i��+&Ղ�ԯ|�,�(M�ՠ�������)�wc�����=t+$�<�:״��-S]�RQ	&�%c��|�N�p��P�	����*E��-K�q`�:&�8���ݥwp_׭kz8D�m�*-��r�Y�3�W6���ߏ���O�CM^C}8�;/�e�)y��xU@F�P�1�i��ʗ��#�.�<.�pq�aX��<�Y��hF�����m�q��#�><��`����ܵfdle��[�R���0;*���-��KD���TW��[��HX�ˌ��:-R��OC��m|�?A�"2�QBI)�".��\�Ё��H����K_G7��@�x�Hi�Q�:���sQT�J��v��l�{���#Ǌ7Z$Z���F]�0�6P��T��:��H��ŝVH��4ai�%Z�R' ����R���Ս������g8-���*��,)繰6�:5)ī	����4J?��Fl �6�����L�l��Q�.8������+���g7��z@��`,��D[e�|-3�+ۿ�R���P�*ԻyUbbi��1��[��{gs������{ߴ0b�i�Mcq,Si�"Q��U�;�l;*��h*��8��	%FD܊6�ѹ�*My�s+6D�]E�<B��#x,�c(wVQ�46���~u�	I�vT,��&����H��j�����J�j.�,Rm�<0K�]op�#�㷡ǫ�� �=�m��IB��Eid�6����7]3c�����<Op\1yZ�fsqǜ	e�u��h��'�_��}�0�
_|���e��l��4ɥ���ͫ������/�}�>���9���ԑ�ŗy]�]��,�*�G.�JQH�W��	�V�[F	��������/�� {      7   �   x���D1C�P���e��c�W.O��8&-=(S�+�xN�;�$=��}bG:p�X҉�$.=�iRx�Y�0���h�/�*�N|p)�
cZ2Rʽ�&��[�yk�H�������&SWsN�M:�!K-��s�Kno�����l��-S����پ���%�ȷH��,�_Gq��W>��������7�     